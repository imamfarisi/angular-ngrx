import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
    {
        path: 'user',
        loadChildren: () => import('./pages/user/user.module').then(u => u.UserModule)
    },
    {
        path: '',
        redirectTo: '/user/list',
        pathMatch: 'full'
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRouter { }