import { NgModule } from "@angular/core";
import { UserSaveComponent } from './user-save/user-save.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserRouter } from "./user.router";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { StoreModule } from "@ngrx/store";
import { userReducer } from "src/app/state/user/user.reducer";
import { EffectsModule } from "@ngrx/effects";
import { UserEffect } from "src/app/state/user/user.effect";

@NgModule({
    declarations: [
        UserSaveComponent,
        UserListComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        UserRouter,
        StoreModule.forFeature('user', userReducer),
        EffectsModule.forFeature([UserEffect])
    ]
})
export class UserModule { }