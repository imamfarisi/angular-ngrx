import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { take } from 'rxjs';
import { User } from 'src/app/dto/user';
import { saveUser, updateUser } from 'src/app/state/user/user.action';
import { userSelectorByUsername } from 'src/app/state/user/user.selector';

@Component({
  selector: 'app-user-save',
  templateUrl: './user-save.component.html',
  styleUrls: ['./user-save.component.css']
})
export class UserSaveComponent implements OnInit {

  user: User = new User()

  constructor(private store: Store, private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.pipe(take(1)).subscribe(result => {
      this.store.select(userSelectorByUsername(result['id'])).pipe(take(1)).subscribe(
        user => {
          this.user = { ...user }
        }
      )
    })
  }

  submit(valid: boolean): void {
    if (valid) {
      if (this.user.id) {
        this.store.dispatch(updateUser({ payload: this.user }))
      } else {
        this.store.dispatch(saveUser({ payload: this.user }))
      }
      alert('Submit successfully')
      this.router.navigateByUrl('/user/list')
    }
  }

}
