import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, take } from 'rxjs';
import { User } from 'src/app/dto/user';
import { deleteUser, loadUser } from 'src/app/state/user/user.action';
import { userSelectorAll, userSelectorInit } from 'src/app/state/user/user.selector';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users$: Observable<User[]> = this.store.select(userSelectorAll)

  constructor(private store: Store, private router: Router) { }

  ngOnInit(): void {
    this.store.select(userSelectorInit).pipe(take(1)).subscribe(result => {
      if (!result) this.store.dispatch(loadUser())
    })
  }

  update(id: number): void {
    this.router.navigateByUrl('/user/' + id)
  }

  delete(id: number): void {
    this.store.dispatch(deleteUser({ payload: id }))
  }
}
