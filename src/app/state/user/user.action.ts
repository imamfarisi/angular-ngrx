import { createAction, props } from "@ngrx/store";
import { User } from "src/app/dto/user";

const loadUser = createAction('[User Page] load user')

const loadUserSuccess = createAction(
    '[User Page] load user success',
    props<{ payload: User[] }>()
)

const saveUser = createAction(
    '[User Page] save user',
    props<{ payload: User }>()
)

const saveUserSuccess = createAction(
    '[User Page] save user success',
    props<{ payload: User }>()
)

const updateUser = createAction(
    '[User Page] update user',
    props<{ payload: User }>()
)

const updateUserSuccess = createAction(
    '[User Page] update user success',
    props<{ payload: User }>()
)

const deleteUser = createAction(
    '[User Page] delete user',
    props<{ payload: number }>()
)

const deleteUserSuccess = createAction(
    '[User Page] delete user success',
    props<{ payload: number }>()
)

export {
    loadUser, loadUserSuccess,
    saveUser, saveUserSuccess,
    updateUser, updateUserSuccess,
    deleteUser, deleteUserSuccess
}