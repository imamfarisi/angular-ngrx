import { createFeatureSelector, createSelector } from "@ngrx/store";
import { User } from "src/app/dto/user";

const userSelectorAll = createSelector(
    createFeatureSelector('user'),
    (state: { payload: User[] }) => state.payload
)

const userSelectorInit = createSelector(
    createFeatureSelector('user'),
    (state: { init: boolean }) => state.init
)

const userSelectorByUsername = (id: number) =>
    createSelector(
        createFeatureSelector('user'),
        (state: { payload: User[] }) => {
            return state.payload.filter(user => user.id == id)[0]
        }
    )

export { userSelectorAll, userSelectorInit, userSelectorByUsername }
