import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, mergeMap } from "rxjs";
import { UserService } from "src/app/service/user.service";
import { deleteUser, deleteUserSuccess, loadUser, loadUserSuccess, saveUser, saveUserSuccess, updateUser, updateUserSuccess } from "./user.action";

@Injectable()
export class UserEffect {

    getData = createEffect(() =>
        this.action$.pipe(
            ofType(loadUser),
            mergeMap(
                () => this.userService.getData().pipe(
                    map(data => loadUserSuccess({ payload: data })),
                )
            )
        )
    )

    saveData = createEffect(() =>
        this.action$.pipe(
            ofType(saveUser),
            mergeMap(
                ({ payload }) => this.userService.saveData(payload).pipe(
                    map(data => saveUserSuccess({ payload: data })),
                )
            )
        )
    )

    updateData = createEffect(() =>
        this.action$.pipe(
            ofType(updateUser),
            mergeMap(
                ({ payload }) => this.userService.updateData(payload).pipe(
                    map(data => updateUserSuccess({ payload: data })),
                )
            )
        )
    )

    deleteData = createEffect(() =>
        this.action$.pipe(
            ofType(deleteUser),
            mergeMap(
                ({ payload }) => this.userService.deleteData(payload).pipe(
                    map(data => deleteUserSuccess({ payload })),
                )
            )
        )
    )

    constructor(private userService: UserService,
        private action$: Actions) { }

}