import { createReducer, on } from "@ngrx/store";
import { User } from "src/app/dto/user";
import { loadUserSuccess, saveUserSuccess, updateUserSuccess, deleteUserSuccess } from "./user.action";

const data: User[] = []

const initialState = {
    payload: data,
    init: false
}

export const userReducer = createReducer(
    initialState,
    on(loadUserSuccess, (state, { payload }) => {
        return { ...state, payload, init: true }
    }),
    on(saveUserSuccess, (state, { payload }) => {
        const newPayload = [...state.payload]
        newPayload.push({ ...payload, id: state.payload.length + 1 })
        const newState = { ...state, payload: newPayload }
        return newState
    }),
    on(updateUserSuccess, (state, { payload }) => {
        const newPayload = state.payload.map(user => {
            const newUser = { ...user }
            if (user.id == payload.id) {
                newUser.id = payload.id
                newUser.username = payload.username
                newUser.address = payload.address
            }
            return newUser
        })
        const newUsers = { ...state, payload: newPayload }
        return newUsers
    }),
    on(deleteUserSuccess, (state, { payload }) => {
        const newPayload = state.payload.filter(user => user.id != payload)
        const newState = { ...state, payload: newPayload }
        return newState
    }),
)