import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { User } from "../dto/user";

@Injectable({
    providedIn: 'root'
})
export class UserService {

    getData(): Observable<User[]> {
        console.log('getting data >>>')
        return of([
            { id: 1, username: 'admin', address: 'Jl. Admin Raya' },
            { id: 2, username: 'non admin', address: 'Jl. Non Admin Raya' },
        ])
    }

    saveData(user: User): Observable<any> {
        console.log('inserting data >>>')
        return of(user)
    }

    updateData(user: User): Observable<any> {
        console.log('updating data >>>')
        return of(user)
    }

    deleteData(id: number): Observable<any> {
        console.log('deleting data >>>')
        return of('success')
    }
}